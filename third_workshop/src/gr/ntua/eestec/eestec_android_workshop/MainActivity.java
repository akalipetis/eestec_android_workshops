package gr.ntua.eestec.eestec_android_workshop;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class MainActivity extends Activity implements OnItemClickListener {

	private FlickrPhotosetAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		ArrayList<FlickrPhotoset> list = new ArrayList<FlickrPhotoset>();
		mAdapter = new FlickrPhotosetAdapter(list, MainActivity.this);
		ListView listView = (ListView) findViewById(R.id.listView1);
		listView.setAdapter(mAdapter);
		listView.setOnItemClickListener(this);

		FlickrPhotosetTask task = new FlickrPhotosetTask();
		task.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View v, int position,
			long id) {

		Intent i = new Intent(this, PhotosetGridActivity.class);
		i.putExtra("photoset_id", id);
		startActivity(i);
	}

	private class FlickrPhotosetTask extends
			AsyncTask<Void, Void, ArrayList<FlickrPhotoset>> {

		@Override
		protected ArrayList<FlickrPhotoset> doInBackground(Void... params) {

			return NetworkHelper.getPhotosets();
		}

		@Override
		protected void onPostExecute(ArrayList<FlickrPhotoset> result) {
			super.onPostExecute(result);

			mAdapter.setPhotosets(result);
		}
	}
}
