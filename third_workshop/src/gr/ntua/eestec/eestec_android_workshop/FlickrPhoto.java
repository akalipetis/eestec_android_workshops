package gr.ntua.eestec.eestec_android_workshop;

public class FlickrPhoto {

	private String mId;
	private String mSecret;
	private String mServer;
	private int mFarm;
	private String mTitle;

	public FlickrPhoto(String id, String secret, String server, int farm,
			String title) {

		mId = id;
		mSecret = secret;
		mServer = server;
		mFarm = farm;
		mTitle = title;
	}

	public String getSmallPhotoUrl() {

		// http://farm1.staticflickr.com/2/1418878_1e92283336_m.jpg

		StringBuilder sb = new StringBuilder("http://farm");
		sb.append(getFarm());
		sb.append(".staticflickr.com/");
		sb.append(getServer());
		sb.append("/");
		sb.append(getId());
		sb.append("_");
		sb.append(getSecret());
		sb.append("_m.jpg");

		return sb.toString();
	}

	public String getId() {

		return mId;
	}

	public String getSecret() {

		return mSecret;
	}

	public String getServer() {

		return mServer;
	}

	public long getFarm() {

		return mFarm;
	}

	public String getTitle() {

		return mTitle;
	}
}
