package gr.ntua.eestec.eestec_android_workshop;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class FlickrPhotosetAdapter extends BaseAdapter {

	private ArrayList<FlickrPhotoset> mPhotosets;
	private final Context mContext;

	public FlickrPhotosetAdapter(ArrayList<FlickrPhotoset> photosets,
			Context context) {

		mPhotosets = photosets;
		mContext = context;
	}

	public void setPhotosets(ArrayList<FlickrPhotoset> photosets) {

		mPhotosets = photosets;

		if (mPhotosets == null)
			notifyDataSetInvalidated();
		else
			notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mPhotosets.size();
	}

	@Override
	public Object getItem(int position) {
		return mPhotosets.get(position);
	}

	@Override
	public long getItemId(int position) {
		return mPhotosets.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		// Replace code with this one to improve speed
		View v;
		if (convertView == null) {

			LayoutInflater infalter = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = infalter.inflate(R.layout.row_view, parent, false);

		} else {

			v = convertView;
		}

		TextView title = (TextView) v.findViewById(R.id.title);
		TextView description = (TextView) v.findViewById(R.id.description);

		title.setText(mPhotosets.get(position).getTitle());
		description.setText(mPhotosets.get(position).getDescription());

		Log.d(String.valueOf(position), mPhotosets.get(position)
				.getDescription());

		return v;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
