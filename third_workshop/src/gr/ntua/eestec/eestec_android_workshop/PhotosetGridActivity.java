package gr.ntua.eestec.eestec_android_workshop;

import java.util.ArrayList;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.GridView;

public class PhotosetGridActivity extends Activity {

	FlickrPhotosAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		long id = getIntent().getLongExtra("photoset_id", 0);

		setContentView(R.layout.activity_photoset_grid);
		GridView grid = (GridView) findViewById(R.id.grid1);

		mAdapter = new FlickrPhotosAdapter(this, new ArrayList<FlickrPhoto>());
		grid.setAdapter(mAdapter);

		FlickrPhotosetPhotosTask task = new FlickrPhotosetPhotosTask();
		task.execute(id);
	}

	private class FlickrPhotosetPhotosTask extends
			AsyncTask<Long, Void, ArrayList<FlickrPhoto>> {

		@Override
		protected ArrayList<FlickrPhoto> doInBackground(Long... params) {

			return NetworkHelper.getPhotosOfPhotoset(params[0]);
		}

		@Override
		protected void onPostExecute(ArrayList<FlickrPhoto> result) {
			super.onPostExecute(result);

			mAdapter.setPhotos(result);
		}
	}
}
