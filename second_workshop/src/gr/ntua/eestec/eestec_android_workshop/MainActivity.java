package gr.ntua.eestec.eestec_android_workshop;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class MainActivity extends Activity implements OnItemClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		ListView listView = (ListView) findViewById(R.id.listView1);

		ArrayList<FlickrPhotoset> list = new ArrayList<FlickrPhotoset>();

		for (int i = 0; i < 100000; ++i) {

			list.add(new FlickrPhotoset(i, "Hello, " + i));
		}

		FlickrPhotosetAdapter adapter = new FlickrPhotosetAdapter(list, this);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View v, int position,
			long id) {

		FlickrPhotosetAdapter adapter = (FlickrPhotosetAdapter) adapterView
				.getAdapter();
		FlickrPhotoset photoset = (FlickrPhotoset) adapter.getItem(position);

		Toast.makeText(this, photoset.getTitle(), Toast.LENGTH_LONG).show();
	}

}
